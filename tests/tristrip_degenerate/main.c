#include <stdio.h>
#include <GL/gl.h>
#include <GL/glu.h>


#define X0 -0.6
#define X1 -0.1
#define Y0 -0.5
#define Y1  0.5

#define X2  0.1
#define X3  0.6

#define Z  -1

GLfloat const vertexData[] = {
	/* 3D Coordinate */
	X1, Y1, Z, // A
	X0, Y1, Z, // B
	X1, Y0, Z, // C
	X0, Y0, Z, // D

	// Cause degenerate triangles
	// in order to create a discontinuity
	X0, Y0, Z, // D
	X3, Y1, Z, // E

	X3, Y1, Z, // E
	X2, Y1, Z, // F
	X3, Y0, Z, // G
};


int initGL() {
	glKosInit();

	glClearColor(0.1, 0.2, 0.4, 1);
	glEnable(GL_CULL_FACE);

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, vertexData);

	return 0;
}

void draw() {
	glClear(GL_COLOR_BUFFER_BIT);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 9);
}

int main(int argc, char **argv) {
	int error;

	puts("main()");

	if(initGL()) {
		puts("Cannot init!");
		return -1;
	}

	while(1) {
		draw();

		glSwapBuffersKOS();

		error = glGetError();
		if(error) {
			printf("OpenGL error: %s\n", gluErrorString(error));
			return -1;
		}
	}

	return 0;
}
